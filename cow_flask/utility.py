from psycopg2 import connect


def create_usertable(user, password, database='cowsay'):
    conn = connect(user=user, password=password, database=database, host='127.0.0.1')
    cur = conn.cursor()
    sql = """ CREATE TABLE CowUser (
              user_id serial,
              name varchar(20) UNIQUE,
              surname varchar(20),
              hashed_password varchar(100)
              )
    """
    cur.execute(sql)
    conn.commit()
    conn.close()


def drop_usertable(user, password, database='cowsay'):
    conn = connect(user=user, password=password, database=database, host='127.0.0.1')
    cur = conn.cursor()
    cur.execute('DROP TABLE CowUser;')
    conn.commit()
    conn.close()


def reset_all_users(user, password, database='cowsay'):
    drop_usertable(user, password, database)
    create_usertable(user, password, database)
