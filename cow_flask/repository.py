import logging

from psycopg2 import connect

from .models import User


POSTRGRES_USER = 'user'
POSTRGRES_PASSWORD = 'user'


class UserRepo():
    def __init__(self):
        logging.debug('Calling UserRepo.__init__()')
        self.conn = connect(user=POSTRGRES_USER, database='cowsay', host='127.0.0.1', password=POSTRGRES_PASSWORD)

    def __enter__(self):
        logging.debug('Calling UserRepo.__enter__()')
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        logging.debug('Calling UserRepo.__exit__()')
        self.conn.close()

    def save(self, user):
        logging.debug('Calling UserRepo.save(), user={}'.format(user))
        cur = self.conn.cursor()

        sql = """ INSERT INTO CowUser (name, surname, hashed_password) VALUES
                  (%(name)s, %(surname)s, %(hashed_password)s);
        """
        cur.execute(sql, {'name': user.name, 'surname': user.surname, 'hashed_password': user.hashed_password})
        self.conn.commit()
        cur.close()

    def update(self, user):
        logging.debug('Calling UserRepo.save(), user={}'.format(user))
        cur = self.conn.cursor()

        sql = """
            START TRANSACTION;
            DELETE FROM CowUser WHERE name = %(name)s;
            INSERT INTO CowUser (name, surname, hashed_password) VALUES
                  (%(name)s, %(surname)s, %(hashed_password)s);
            COMMIT;

        """
        cur.execute(sql, {'name': user.name, 'surname': user.surname, 'hashed_password': user.hashed_password})
        self.conn.commit()
        cur.close()

    def get_user(self, username):
        logging.debug('Running UserRepo.get_user(), username={}'.format(username))
        sql = """
            SELECT * FROM CowUser WHERE name=%(username)s;
        """
        cur=self.conn.cursor()
        cur.execute(sql, {'username': username})
        row = cur.fetchone()
        logging.debug('UserRepo "row" is: {}'.format(row))
        if not row:
            raise Exception('User not found!')
        u = User(name=row[1], surname=row[2], password=None)
        u.hashed_password = row[3]
        cur.close()
        logging.debug('UserRepo.get_user() finished')
        return u

    def get_all_users(self):
        logging.debug('Running UserRepo.get_all_users()')
        sql = """
            SELECT * FROM CowUser;
        """
        cur = self.conn.cursor()
        cur.execute(sql)
        all_users = []
        for row in cur.fetchall():
            all_users.append([str(elem) for elem in row])
        cur.close()
        logging.debug('UserRepo.get_all_users() finished, fetched {} rows'.format(len(all_users)))
        return all_users

