#!/usr/bin/env python

import os
import logging
from flask import Flask, request, redirect, url_for, render_template
from flask_login import LoginManager, login_required, login_user, logout_user, current_user

from .forms import UpdateUserForm, LoginForm, RegisterForm
from .models import User
from .repository import UserRepo, POSTRGRES_USER, POSTRGRES_PASSWORD
from .utility import reset_all_users


logging.basicConfig(level=logging.DEBUG)

app = Flask(__name__)

app.config.update(dict(
    SECRET_KEY='very secret'
))

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "sign_in"



@app.before_request
def before_request():
    logging.debug('Current user is {}'.format(current_user))

@login_manager.user_loader
def load_user(user_id):
    """ Our user_id is user's name """
    logging.debug('Running load_user(user_id={})'.format(user_id))
    try:
        ur = UserRepo()
        return ur.get_user(user_id)
    except Exception as e:
        logging.error(str(e))
        return None


@app.route('/', methods=['GET', 'POST'])
def index():
    logging.debug('Visiting /index/')
    return render_template('index.html')


@app.route('/sign-in/', methods=['GET', 'POST'])
def sign_in():
    logging.debug('Visiting /sign-in/')
    if current_user is not None and current_user.is_authenticated:
        return redirect(url_for('welcome'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User(form.data['firstname'])
        login_user(user)

        next = request.args.get('next')

        return redirect(next or url_for('index'))
    return render_template('sign-in.html', form=form)


@app.route('/sign-up/', methods=['GET', 'POST'])
def sign_up():
    logging.debug('Visiting /sign-up/')
    if current_user is not None and current_user.is_authenticated:
        return redirect(url_for('welcome'))
    form = RegisterForm()
    if form.validate_on_submit():
        firstname = form.data['firstname']
        lastname = form.data['lastname']
        password = form.data['password']
        ur = UserRepo()
        user = User(name=firstname, surname=lastname, password=password)
        ur.save(user)
        login_user(user)
        return redirect(url_for('sign_in'))
    return render_template('sign-up.html', form=form)


@app.route("/logout")
@login_required
def logout():
    logging.debug('Logging out...')
    logout_user()
    return redirect(url_for('index'))


@app.route('/welcome/')
@login_required
def welcome():
    logging.debug('Visiting /welcome/')
    with UserRepo() as ur:
        users = ur.get_all_users()
    return render_template('welcome.html', users=users)


@app.route('/profile/',  methods=['GET', 'POST'])
@login_required
def profile():
    logging.debug('Visiting /profile/')
    form = UpdateUserForm()
    if form.validate_on_submit():
        lastname = form.data['lastname']
        password = form.data['password']
        with UserRepo() as ur:
            user = current_user
            if lastname:
                user.surname = lastname
            if password:
                user.new_password(password)
            ur.update(user)
        return redirect(url_for('index'))
    form.lastname.default = current_user.surname
    form.process()
    return render_template('profile.html', form=form)

@app.route('/reset/', methods=['GET', 'POST'])
@login_required
def reset_users():
    logging.debug('Visiting /reset/')
    reset_all_users(POSTRGRES_USER, POSTRGRES_PASSWORD)
    logout_user()
    return render_template('index.html')



@app.errorhandler(404)
def page_not_found(e):
    logging.debug('Visiting 404')
    return render_template('404.html'), 404

