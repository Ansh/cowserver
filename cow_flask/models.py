import logging

from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash


class User(UserMixin):
    def __init__(self, name, surname=None, password=None):
        self._name = name
        self._surname = surname
        if password:
            self.hashed_password = generate_password_hash(password)
        else:
            self.hashed_password = None

    def get_id(self):
        logging.debug('Running User.get_id()')
        return self._name

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    @property
    def surname(self):
        return self._surname

    @surname.setter
    def surname(self, surname):
        self._surname = surname

    @property
    def password(self):
        raise AttributeError('Protected field!')

    def new_password(self, password):
        self.hashed_password = generate_password_hash(password)

    def is_password_valid(self, password):
        if self.hashed_password:
            return check_password_hash(self.hashed_password, password)
        raise Exception('User has no password!')

    @property
    def is_authenticated(self):
        logging.debug('Running User.is_authenticated')
        return True

    @property
    def is_active(self):
        logging.debug('Running User.is_active')
        return True

    @property
    def is_anonymous(self):
        logging.debug('Running User.is_anonymous')
        return False

    def __repr__(self):
        return 'User(firstname="{}", lastname="{}")'.format(self.name, self.surname)
