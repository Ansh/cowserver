import logging

from flask_wtf import Form
from wtforms import StringField, PasswordField, validators, ValidationError

from .repository import UserRepo


class LoginForm(Form):
    firstname = StringField('First name', [validators.DataRequired(), validators.Length(min=1, max=50)])
    password = PasswordField('Password', [validators.DataRequired(), validators.Length(min=1, max=50)])

    def validate_password(form, field):
        logging.debug("LoginForm password validation, name: {} password: {} ".format(
            form.firstname.data, form.password.data
        ))
        ur = UserRepo()
        try:
            user = ur.get_user(form.firstname.data)
        except Exception as e:
            raise ValidationError(str(e))
        if not user.is_password_valid(form.password.data):
            raise ValidationError('Wrong login/password!')


class RegisterForm(Form):
    firstname = StringField('First name', [validators.Length(min=1, max=50)])
    lastname = StringField('Last name', [validators.Length(min=1, max=50)])
    password = PasswordField('Password', [
        validators.DataRequired(),
        validators.Length(min=1, max=50),
        validators.EqualTo('confirm', message='Passwords must match')
    ])
    confirm = PasswordField('Repeat Password')

    def validate_firstname(form, field):
        ur = UserRepo()
        try:
            u = ur.get_user(field.data)
        except Exception as e:
            if not str(e) == 'User not found!':
                raise ValidationError(str(e))
            return
        if u:
            raise ValidationError('A user with this login has already registered!')


class UpdateUserForm(Form):
    lastname = StringField('Change last name', [validators.Length(max=50)])
    password = PasswordField('Change password', [
        validators.Length(max=50),
        validators.EqualTo('confirm', message='Passwords must match')
    ])
    confirm = PasswordField('Repeat Password')

